### 概述
&nbsp;&nbsp;&nbsp;TityPlatform是基于SpringBoot的企业应用开发框架。TityPlatform以[TitySQL](https://gitee.com/LeeFJ/tity-sql-public/wikis/pages)作为DAO层，它和TitySQL一样，倡导基于数据的快速开发。TityPlatform在框架中落地适量、适当的规范，以提高业务构建和编码效率。
### 理念

&nbsp;&nbsp;&nbsp;TityPlatform希望开发人员，更多关注业务本身，而非技术，以业务利落体现系统价值和自身价值。TityPlatform的用户是项目技术人员，并非系统的最终用户。TityPlatform的架构设计简单轻巧，技术基础设施与业务基础设施丰富，为企业应用开发提供一站式解决方案。

&nbsp;&nbsp;&nbsp;使用TityPlatform使团队中，常规开发人员主要关注业务模块(MVC)的代码实现,以及基础设施的使用。架构师则需要对底层适当了解，选择合适的基础设施作为业务实现的技术组件。

&nbsp;&nbsp;&nbsp;TityPlatform已经为企业应用整合了必要的技术工具，开发团队仅需按照TitySQL的规范建立表结构，生成实体、生成业务模块代码(MVC)后，以业务填空的方式完成功能代码编写。TityPlatform针对单记录表单、多记录列表、层次化的树形结构分别提供三种不同的MVC代码结构。60%以上的常规业务逻辑以填空的方式即可实现，复杂的业务逻辑也可轻松扩展。

#### 进入 [TityPlatform Wiki](https://gitee.com/LeeFJ/tity_platform/wikis/pages)



